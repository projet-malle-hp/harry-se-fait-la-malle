tiroir_caisse_guipure = [
    {"valeur": 200, "quantite": 1},
    {"valeur": 100, "quantite": 3},
    {"valeur": 50, "quantite": 1},
    {"valeur": 20, "quantite": 1},
    {"valeur": 10, "quantite": 1},
    {"valeur": 5, "quantite": 1},
    {"valeur": 2, "quantite": 5}
]

def rendre_monnaie(somme_a_rendre, tiroir_caisse):
    tiroir_caisse = sorted(tiroir_caisse, key=lambda x: x["valeur"], reverse=True)
    meilleure_combinaison = None
    meilleure_perte = float('inf')
    
    def trouver_combinaison(restant, index, rendu_actuel, perte_actuelle):
        nonlocal meilleure_combinaison, meilleure_perte
        if restant == 0 and perte_actuelle < meilleure_perte:
            meilleure_combinaison = rendu_actuel.copy()
            meilleure_perte = perte_actuelle
            return

        if index == len(tiroir_caisse):
            return

        billet_piece = tiroir_caisse[index]
        valeur = billet_piece["valeur"]
        quantite = billet_piece["quantite"]

        for quantite_rendue in range(min(restant // valeur, quantite), -1, -1):
            if quantite_rendue > 0:
                rendu_actuel[valeur] = rendu_actuel.get(valeur, 0) + quantite_rendue
                billet_piece["quantite"] -= quantite_rendue

            trouver_combinaison(restant - quantite_rendue * valeur, index + 1, rendu_actuel, perte_actuelle + quantite_rendue * valeur)

            if quantite_rendue > 0:
                billet_piece["quantite"] += quantite_rendue
                rendu_actuel[valeur] -= quantite_rendue
                if rendu_actuel[valeur] == 0:
                    del rendu_actuel[valeur]

    trouver_combinaison(somme_a_rendre, 0, {}, 0)

    return meilleure_combinaison

sommes_a_rendre_guipure = [0, 17, 68, 231, 497, 842]

for somme in sommes_a_rendre_guipure:
    rendu = rendre_monnaie(somme, tiroir_caisse_guipure.copy())

    if rendu is not None:
        pertes = somme - sum(valeur * quantite for valeur, quantite in rendu.items())
        print(f"Somme à rendre : {somme} | Rendu : {rendu} | Pertes : {pertes}")
        print("--------")
    else:
        rendu_sup = rendre_monnaie(somme + 1, tiroir_caisse_guipure.copy())
        if rendu_sup is not None:
            pertes_sup = (somme + 1) - sum(valeur * quantite for valeur, quantite in rendu_sup.items())
            print(f"Somme à rendre : {somme} | Rendu : {rendu_sup} | Pertes : {pertes_sup}")
            print("--------")
        else:
            print("Désolé, la caisse n'a pas assez d'argent, je vous la laisse entièrement. Veuillez repasser demain.")
            print("--------")