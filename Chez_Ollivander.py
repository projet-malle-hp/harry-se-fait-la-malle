def rendre_monnaie_en_euros(somme_a_rendre):
# Valeurs des piÃ¨ces et billets disponibles en euros
    valeurs = [500, 200, 100, 50, 20, 10, 5, 2, 1]

    rendu_monnaie = {}

    for valeur in valeurs:
        if somme_a_rendre >= valeur:
# Calculer le nombre de piÃ¨ces/billets
            nombre = somme_a_rendre // valeur

# Mettre Ã  jour la somme Ã  rendre
            somme_a_rendre %= valeur

# Stocker le nombre dans le dictionnaire
            rendu_monnaie[valeur] = nombre

    return rendu_monnaie

# Nouvelle fonction pour calculer le rendu de monnaie en Gallions, Mornilles et Noises
def rendre_monnaie_en_gallions_mornilles_noises(somme_a_rendre):
    # Valeurs en euros équivalentes
    valeur_gallion = 17 * 29  # Valeur en euros d'un Gallion
    valeur_mornille = 29  # Valeur en euros d'une Mornille
    valeur_noise = 1  # Valeur en euros d'une Noise

    # Convertir la somme à rendre en euros
    somme_en_euros = somme_a_rendre / 100

    # Calculer le rendu en Gallions, Mornilles et Noises
    rendu_gallions = somme_en_euros // valeur_gallion
    reste_gallions = somme_en_euros % valeur_gallion

    rendu_mornilles = reste_gallions // valeur_mornille
    reste_mornilles = reste_gallions % valeur_mornille

    rendu_noises = reste_mornilles // valeur_noise

    return int(rendu_gallions), int(rendu_mornilles), int(rendu_noises)

# Tester la fonction avec différentes sommes à rendre
sommes_a_rendre = [0, 654, 23 * 29 + 78, 7 * 17 * 29 + 11 * 29 + 9]
for somme in sommes_a_rendre:
    rendu_gallions, rendu_mornilles, rendu_noises = rendre_monnaie_en_gallions_mornilles_noises(somme)
    print(f"Sommme à rendre : {somme} euros | Rendu : {rendu_gallions} Gallions, {rendu_mornilles} Mornilles, {rendu_noises} Noises")
    print("--------")