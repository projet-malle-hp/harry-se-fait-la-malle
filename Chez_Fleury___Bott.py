def rendre_monnaie_en_euros(somme_a_rendre):
# Valeurs des pièces et billets disponibles en euros
    valeurs = [500, 200, 100, 50, 20, 10, 5, 2, 1]

    rendu_monnaie = {}

    for valeur in valeurs:
        if somme_a_rendre >= valeur:
# Calculer le nombre de pièces/billets
            nombre = somme_a_rendre // valeur
            somme_a_rendre %= valeur
# Stocker le nombre dans le dictionnairez
            rendu_monnaie[valeur] = nombre

    return rendu_monnaie

sommes_a_rendre = [0, 60, 63, 231, 899]

for somme in sommes_a_rendre:
    rendu = rendre_monnaie_en_euros(somme)
    print(f"Somme � rendre : {somme}")
    print("Rendu de monnaie : ", rendu)
    print("--------")